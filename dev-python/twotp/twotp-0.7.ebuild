# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"
SUPPORT_PYTHON_ABIS="1"

inherit distutils

DESCRIPTION="Python implementation of the Erlang node protocol"
HOMEPAGE="http://launchpad.net/twotp"
SRC_URI="http://launchpad.net/twotp/trunk/${PV}/+download/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="dev-python/twisted"
RDEPEND="${DEPEND}"

DOCS="doc/intro.xhtml"
