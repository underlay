# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit multilib qt4

DESCRIPTION="Thick abstraction layer for audio/video RTP services"
HOMEPAGE="http://delta.affinix.com/psimedia/"
SRC_URI="http://delta.affinix.com/download/${PN}/${P}.tar.bz2"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug demo"

DEPEND=">=dev-libs/glib-2.18
	>=dev-libs/liboil-0.3
	>=media-libs/gst-plugins-base-0.10.22
	>=media-libs/gst-plugins-good-0.10.14
	>=media-libs/speex-1.2_rc1
	>=x11-libs/qt-gui-4.4:4[glib]"
RDEPEND="${DEPEND}
	>=media-plugins/gst-plugins-alsa-0.10.22
	>=media-plugins/gst-plugins-jpeg-0.10.14
	>=media-plugins/gst-plugins-ogg-0.10.22
	>=media-plugins/gst-plugins-speex-0.10.14
	>=media-plugins/gst-plugins-theora-0.10.22
	>=media-plugins/gst-plugins-v4l-0.10.22
	>=media-plugins/gst-plugins-v4l2-0.10.14
	>=media-plugins/gst-plugins-vorbis-0.10.22
	!<net-im/psi-0.13_rc1"

src_prepare() {
	sed -i -e '/^TEMPLATE/a CONFIG += ordered' psimedia.pro || die

	if ! use demo; then
		sed -i -e '/^SUBDIRS[[:space:]]*+=[[:space:]]*demo[[:space:]]*$/d' \
			psimedia.pro || die
	fi
}

src_configure() {
	# cannot use econf because of non-standard configure script
	local confcmd="./configure
			--qtdir=/usr
			--no-separate-debug-info
			--$(use debug && echo debug || echo release)"

	echo ${confcmd}
	${confcmd} || die "configure failed"

	eqmake4
}

src_install() {
	exeinto /usr/$(get_libdir)/psi/plugins
	doexe gstprovider/libgstprovider.so || die

	if use demo; then
		exeinto /usr/$(get_libdir)/${PN}
		newexe demo/demo ${PN} || die

		# Create /usr/bin/psimedia
		cat <<-EOF > "demo/${PN}"
		#!/bin/sh

		export PSI_MEDIA_PLUGIN=/usr/$(get_libdir)/psi/plugins/libgstprovider.so
		/usr/$(get_libdir)/${PN}/${PN}
		EOF

		dobin demo/${PN} || die
	fi
}
