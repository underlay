# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit nsplugins

DESCRIPTION="Video chat browser plug-in for Google Talk"
HOMEPAGE="http://www.google.com/chat/video"
BASE_SRC_URI="http://dl.google.com/linux/direct/${PN}_current"
SRC_URI="amd64? ( ${BASE_SRC_URI}_amd64.deb )
	x86? ( ${BASE_SRC_URI}_i386.deb )"
LICENSE=""

SLOT="0"
KEYWORDS="-* ~amd64"
IUSE=""
RESTRICT="mirror strip"

QA_TEXTRELS_amd64="opt/google/talkplugin/libnpgtpo3dautoplugin.so
		opt/google/talkplugin/libnpgoogletalk64.so"
QA_TEXTRELS_x86="opt/google/talkplugin/libnpgtpo3dautoplugin.so
		opt/google/talkplugin/libnpgoogletalk.so"

DEPEND=""
RDEPEND="
	>=dev-libs/glib-2.12:2
	media-gfx/nvidia-cg-toolkit
	>=media-libs/fontconfig-2.4.0
	>=media-libs/freetype-2.3.5
	media-libs/libpng:1.2
	sys-apps/lsb-release
	virtual/opengl
	>=x11-libs/gtk+-2.12:2
	x11-libs/libX11
	x11-libs/libXfixes
	x11-libs/libXt
	amd64? (
		app-emulation/emul-linux-x86-baselibs
		app-emulation/emul-linux-x86-soundlibs
		app-emulation/emul-linux-x86-xlibs
	)"

src_unpack() {
	unpack ${A}
	unpack ./data.tar.gz
}

src_install() {
	cd "${WORKDIR}"/opt/google/talkplugin
	exeinto /opt/google/talkplugin
	doexe GoogleTalkPlugin || die
	for x in npgtpo3dautoplugin npgoogletalk$(use amd64 && echo 64); do
		doexe lib${x}.so || die
		inst_plugin /opt/google/talkplugin/lib${x}.so || die
	done
}
